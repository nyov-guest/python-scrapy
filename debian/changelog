python-scrapy (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0
  * Update patches.
  * Bump Standards-Version to 4.3.0.

 -- nyov <nyov@nexnode.net>  Sun, 10 Feb 2019 19:42:54 +0000

python-scrapy (1.5.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ nyov ]
  * New upstream release. (Closes: #891725)
  * Clean up alternatives on removal. (Closes: #855578)
  * Install lintian override to silence "source is missing" on
    overlong javascript line.

  [ Andrej Shadura ]
  * Make Python 2 packages depend on python-functools32.
  * Catch StopIteration from raised from next().
    Generators should not raise StopIteration, but return instead.
    Python 3.7 enforces that.

 -- Andrej Shadura <andrewsh@debian.org>  Sat, 29 Sep 2018 14:51:15 +0200

python-scrapy (1.5.0-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Always use pristine-tar.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.3.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Enable autopkgtest-pkg-python testsuite.
  * Remove debian/python-scrapy.examples as upstream no longer provides
    the related files.
  * Build documentation in override_dh_sphinxdoc.
  * Remove trailing whitespaces in previous changelog entries.

 -- Michael Fladischer <fladi@debian.org>  Sun, 21 Jan 2018 21:30:28 +0100

python-scrapy (1.4.0-1) unstable; urgency=low

  * Upload to unstable (Closes: #871385).
  * Team upload.
  * Refresh patches after git-dpm to gbp pq conversion
  * Bump Standards-Version to 4.1.0.

 -- Michael Fladischer <fladi@debian.org>  Tue, 29 Aug 2017 22:06:51 +0200

python-scrapy (1.4.0-1~exp1) experimental; urgency=low

  * Team upload.
  * New upstream release.
  * Require twisted (>= 17.1.0) for Build-Depends so FTP tests can run.
  * Reformat packaging files with cme for better readability.
  * Bump Standards-Version to 4.0.0.

 -- Michael Fladischer <fladi@debian.org>  Tue, 20 Jun 2017 11:44:41 +0200

python-scrapy (1.3.3-1~exp1) experimental; urgency=low

  * Team upload.
  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Fri, 17 Mar 2017 09:00:55 +0100

python-scrapy (1.3.2-1~exp1) experimental; urgency=low

  * Team upload.
  * New upstream release.
  * Drop patch applied upstream.

 -- Michael Fladischer <fladi@debian.org>  Fri, 17 Feb 2017 09:52:04 +0100

python-scrapy (1.3.0-1~exp2) experimental; urgency=low

  * Team upload.
  * Switch to pybuild and add all packages required to run the tests to
    Build-Depends-Indep.
  * Add Python3 support through separate binary package.
  * Use alternatives system to manage /usr/bin/scrapy, its manpage and
    bash completion, to either utilize Python2 or Python3 with the later
    taking precedence if installed.
  * Add NEWS file to document alternatives for /usr/bin/scrapy.
  * Recreate cert.pem before running the tests to avoid errors with
    OpenSSL 1.1 because of weak keys (512bit).
  * Install README.rst in both module packages.
  * Switch to python3-sphinx.
  * Install upstream changelog and remove lintian overrides.
  * Remove README.Debian as upstream no longer ships embedded libraries.

 -- Michael Fladischer <fladi@debian.org>  Thu, 02 Feb 2017 13:40:12 +0100

python-scrapy (1.3.0-1~exp1) experimental; urgency=low

  * Team upload.
  * New upstream release.
  * Clean files in Scrapy.egg-info/ to allow two builds in a row.
  * Use https:// for copyright-format 1.0 URL.
  * Use BSD-3-clause instead of BSD-3 in d/copyright.
  * Use Expat instead of MIT in d/copyright.
  * Replace versioned python with python-all in Build-Depends.
  * Remove python-guppy from Recommends.
  * Drop python-django from Recommends as Django support was moved to a
    separate project.
  * Bump debhelper compatibility and version to 9.
  * Drop python-simplejson from Recommends as it is no longer imported
    anywhere in the code.
  * Reformat packaging files with cme for better readability.
  * Remove unnecessary Provides field.
  * Remove unused sections and licenses from d/copyright as upstream has
    stopped shipping embedded libraries in their distributions.
  * Add patch to fix spelling error in manpage.
  * Add python-parsel to Build-Depends.

 -- Michael Fladischer <fladi@debian.org>  Wed, 25 Jan 2017 10:29:07 +0100

python-scrapy (1.0.3-2) unstable; urgency=medium

  * Team upload.

  [ Dmitry Shachnev ]
  * Use dh_sphinxdoc to link shared documentation files.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Vincent Cheng ]
  * Add patch to fix FTBFS with sphinx 1.4. (Closes: #830263)
  * Update Standards version to 3.9.8.

 -- Vincent Cheng <vcheng@debian.org>  Sun, 10 Jul 2016 23:03:05 -0700

python-scrapy (1.0.3-1) unstable; urgency=medium

  * Fresh upstream release.
    Tested that new project could be created (Closes: #755732)
  * Do install .egg so that scrapy entry point could find scrapy module
    (Closes: #797364)
  * debian/control
    - dh-python to Build-Depends (thanks lintian)
    - moved python-lxml to Depends from Recommends (Closes: #797365)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 02 Sep 2015 21:36:10 -0400

python-scrapy (1.0.0-1) unstable; urgency=medium

  * Team upload
  * Fresh upstream release (Closes: #789606)
  * debian/watch
    - use pypi.debian.net redirector to fetch the sources
  * debian/
    - do not deal with scrapy-ws.py script any longer -- was moved to
      scrapy-jsonrpc package (N/A from Debian archive ATM)
  * debian/rules
    - make build "reproducible" by fixing date to the one from
      debian/changelog (Closes: #788593). Thanks Juan Picca for the patch!
  * debian/control
    - minimal required version of w3lib boosted to 1.8
    - Build-Depends on python-setuptools and python-sphinx-rtd-theme
    - boost policy to 3.9.6
  * debian/copyright
    - moved verbatim of BSD-3 and MIT licenses into separate paragraphs
      for reuse
  * debian/python-scrapy-doc.doc-base
    - removed obsolete reference to /experimental/*.html
  * debian/patches (sent upstream)
    - changeset_48582be.diff - explicit path to RTD sphinx theme
    - changeset_49fe915.diff - do not ignore failed docs build

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 25 Jun 2015 10:53:02 -0400

python-scrapy (0.24.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Depend on python-cssselect. (Closes: #748864)

 -- Vincent Cheng <vcheng@debian.org>  Sat, 19 Jul 2014 23:18:07 -0700

python-scrapy (0.22.2-3) unstable; urgency=medium

  * Team upload.
  * Depend on python-w3lib >= 1.2. (Closes: #743315)

 -- Vincent Cheng <vcheng@debian.org>  Thu, 03 Apr 2014 23:58:42 -0700

python-scrapy (0.22.2-2) unstable; urgency=medium

  * Team upload.
  * Depend on python-queuelib. (Closes: #743199)

 -- Vincent Cheng <vcheng@debian.org>  Mon, 31 Mar 2014 15:32:18 -0700

python-scrapy (0.22.2-1) unstable; urgency=medium

  * Team upload.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Remove DM-Upload-Allowed; it's no longer used by the archive
    software.

  [ Vincent Cheng ]
  * New upstream release. (Closes: #742983)
    - Drop patch, applied upstream.
  * Update Standards version to 3.9.5.

 -- Vincent Cheng <vcheng@debian.org>  Sun, 30 Mar 2014 23:32:02 -0700

python-scrapy (0.14.4-1) unstable; urgency=low

  * New upstream release.
  * debian/patches:
    - 01_ftbs_easy_query.diff: corrects FTBS caused by the urlparse
    Python module import. (Closes: #678792)
  * debian/rules:
    - Added exclude on *.egg files.

 -- Ignace Mouzannar <mouzannar@gmail.com>  Thu, 28 Jun 2012 23:41:01 +0400

python-scrapy (0.14.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Added dependency on python-w3lib.
  * debian/copyright:
    - Updated formet to fit the dep5 specification.
    - Added copyright information for file scrapy/xlib/ordereddict.py.
  * debian/python-scrapy.lintian-overrides:
    - Removed override for file googledir/scrapy.cfg.

 -- Ignace Mouzannar <mouzannar@gmail.com>  Sun, 12 Feb 2012 23:45:47 +0400

python-scrapy (0.12.0.2546-1) unstable; urgency=low

  * New upstream release:
    - Includes the patch adding iPython-0.11 compatibility. Thank you
      Julian Taylor <jtaylor.debian@googlemail.com> for providing the patch.
      (Closes: #636719)
  * debian/control:
    - Added dependency on libjs-underscore.
    - Removed dependency on python-support.
    - Bumped minimum required python dependency to 2.6.6-3~.
    - Added X-Python-Version.
  * debian/rules:
    - Added python2 to dh's --with argument.
  * debian/pyversions:
    - Deleted file.
  * debian/python-scrapy-doc.links:
    - Added link to underscore.js library.
  * debian/python-scrapy.lintian-overrides:
    - Added overrides for false-positive duplicate file.
  * debian/python-scrapy-doc.lintian-verrides:
    - Added override for the missing changelog in the -doc package.

 -- Ignace Mouzannar <mouzannar@gmail.com>  Mon, 29 Aug 2011 03:07:25 +0200

python-scrapy (0.12.0.2542-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Bumped Standards-Version to 3.9.2.
  * debian/pyversions:
    - Added build support for Python 2.7. (Closes: #629091)

 -- Ignace Mouzannar <mouzannar@gmail.com>  Sat, 04 Jun 2011 19:28:35 +0200

python-scrapy (0.12.0.2540-1) unstable; urgency=low

  * New upstream release (Closes: #619352)
  * debian/control:
    - Added the "DM-Upload-Allowed" field.

 -- Ignace Mouzannar <mouzannar@gmail.com>  Fri, 22 Apr 2011 18:36:00 +0200

python-scrapy (0.12.0.2538-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Removed build dependency on python-jinja. (Closes: #613282)
    - Moved the Python dependency from Build-Depends-Indep to Build-Depends.
      (Closes: #613281)

 -- Ignace Mouzannar <mouzannar@gmail.com>  Sun, 27 Feb 2011 19:35:57 +0100

python-scrapy (0.12.0.2528-1) unstable; urgency=low

  * New upstream version.
  * debian/rules:
    - Removed references to scrapy-ctl.py and scrapy-sqs.py.
    - Added bash-completion support.
    - Added exclusion of "objects.inv" in override_dh_compress.
      (Closes: #608774)
  * debian/control:
    - Added Build-Depends on bash-completion.
  * debian/scrapy-ctl.1, debian/scrapy-sqs.1:
    - Removed files, as their respective binary files are not distributed by
      upstream anymore.
  * debian/python-scrapy.install:
    - Added the installation of scrapy-ws.py.
  * debian/python-scrapy.manpages:
    - Removed references to scrapy-ctl.1 and scrapy-sqs.1.
    - Added scrapy.1 manpage.
  * debian/watch:
    - Updated the file to point to the new PyPI repository. Thank you Janos
      Guljas <janos@resenje.org> for the patch.
  * debian/python-scrapy.bash-completion:
    - New file pointing to the bash-completion file distributed by upstream.
  * debian/pytnon-scrapy.lintian-override:
    - Added override of "no-upstream-changelog" warning, as upstream does not
      provide any changelog.
  * debian/python-scrapy.README.Debian:
    - Added a mention about BeautifulSoup and ClientForm embedded libraries.
  * debian/changelog:
    - Setting the package back to unstable, as this is upstream's stable
      version and no freeze exception will be requested for it.

 -- Ignace Mouzannar <mouzannar@gmail.com>  Thu, 27 Jan 2011 12:15:03 +0100

python-scrapy (0.9-1) experimental; urgency=low

  * New upstream release.
  * debian/patches:
    - 02_view_response.diff: Removed this patch as it has been applied upstream.
    - 01_external_modules.diff: Removed this patch as upstream clearly pointed
      out the need to use the embedded copies of these external modules instead
      of the ones delivered by the Debian packages.
      For more details: http://dev.scrapy.org/ticket/129
  * debian/control:
    - Bumped Standards-Version to 3.9.1.
    - Moved python-boto from Recommends to Depends as it is now needed by the
      scrapy-sqs script.
    - Removed dependencies on python-beautifulsoup and python-clientform as
      the embedded modules in scrapy/xlib/ will be used instead.
  * debian/copyright:
    - Removed reference to deprecated /usr/share/common-licenses/BSD.
  * debian/rules:
    - Updated to remove language extensions (.py) of new scripts added by
      upstream.
    - Removed the flag used to ignore BeautifulSoup.py and ClientForm.py.
  * debian/scrapy-sqs.1 & debian/scrapy-ws.1
    - New manpages for scrapy-sqs and scrapy-ws.
  * debian/copyright:
    - Added copyright information for folder scrapy/xlib/simplejson/

 -- Ignace Mouzannar <mouzannar@gmail.com>  Sun, 22 Aug 2010 22:52:30 +0200

python-scrapy (0.8-3) unstable; urgency=low

  * debian/watch:
    - Corrected regular expression to catch "Scrapy" and "scrapy".
  * debian/patches:
    - 01_external_modules.diff: refreshed patch.
    - 02_view_response.diff: fixes bug in open_in_browser() function with
      Python 2.5. (Closes: #573369).
  * debian/control:
    - Bumped Standards-Version to 3.8.4.
  * debian/python-scrapy.lintian-overrides:
    - Added overrides for 'duplicated-compressed-file' as the two suspected
      files are intentionally present and used by the scrapy test suite.

 -- Ignace Mouzannar <mouzannar@gmail.com>  Thu, 13 May 2010 14:59:09 +0200

python-scrapy (0.8-2) unstable; urgency=low

  * debian/control:
    - Added dependency on python-twisted-mail.

 -- Ignace Mouzannar <mouzannar@gmail.com>  Mon, 14 Dec 2009 22:13:54 +0000

python-scrapy (0.8-1) unstable; urgency=low

  * New upstream release.
  * debian/patches:
    - 01_external_modules.diff: refreshed content as some files were modified
      in the new upstream version.

 -- Ignace Mouzannar <mouzannar@gmail.com>  Mon, 14 Dec 2009 21:26:16 +0000

python-scrapy (0.7-2) unstable; urgency=low

  * Converted the package to "3.0 (quilt)" format.
  * debian/control:
    - Changed Build dependency from python-all-dev to python.
    - Added dependency on python-beautifulsoup and python-clientform.
    - Added Recommends field listing listing all the non absolute dependencies.
    - Moved python-simplejson dependency from Suggests to Recommends.
    - Changed dependecy on python-twisted which is "extra" to
      python-twisted-core, python-twisted-web and python-twisted-conch.
  * debian/rules:
    - Now excludes BeautifulSoup.py and ClientForm.py.
  * debian/patches:
    - 01_external_modules.diff: Updated imports of BeautifulSoup and ClientForm
      to point to the main Python modules.
  * debian/source/format:
    - New file set to "3.0 (quilt)".

 -- Ignace Mouzannar <mouzannar@gmail.com>  Tue, 24 Nov 2009 00:07:21 +0100

python-scrapy (0.7-1) unstable; urgency=low

  * Initial release. (Closes: #551038)

 -- Ignace Mouzannar <mouzannar@gmail.com>  Thu, 19 Nov 2009 23:25:57 +0100
